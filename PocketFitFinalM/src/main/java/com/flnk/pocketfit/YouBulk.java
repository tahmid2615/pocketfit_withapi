package com.flnk.pocketfit;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.flnk.pocketfit.database.DBHandler;

import java.util.ArrayList;


public class YouBulk extends AppCompatActivity {





    private static final String TAG = "You";

    DBHandler dbHandler;
    EditText etnamebulk, etagebulk, etheightbulk, etweightbulk;

    RedirectActivity redirectActivity;
    NxFitbitHelper nxFitbitHelper;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_youbulk);


        dbHandler = new DBHandler(this);


        //setting values of text to user

        //NAME
        ArrayList<String> list1 = dbHandler.getName();
        String Namelist = String.join(", ", list1);
        TextView username = (TextView) this.findViewById(R.id.nameviewyoubulk);
        username.setText(String.valueOf(Namelist));

        //AGE
        ArrayList<String> list2 = dbHandler.getAge();
        String Agelist = String.join(", ", list2);
        TextView yourage = this.findViewById(R.id.ageviewyoubulk);
        yourage.setText(String.valueOf(Agelist));

        //HEIGHT
        ArrayList<String> list3 = dbHandler.getHeight();
        String Heightlist = String.join(", ", list3);
        TextView yourheight = this.findViewById(R.id.heightviewyoubulk);
        yourheight.setText(String.valueOf(Heightlist));

        //WEGIHTR
        ArrayList<String> list5 = dbHandler.getWeight();
        String Weightlist = String.join(", ", list5);
        TextView yourweight = this.findViewById(R.id.weightviewyoubulk);
        yourweight.setText(String.valueOf(Weightlist));


        //lock string to text boxes for pull
        etnamebulk = findViewById(R.id.nameviewyoubulk);
        etagebulk = findViewById(R.id.ageviewyoubulk);
        etheightbulk = findViewById(R.id.heightviewyoubulk);
        etweightbulk = findViewById(R.id.weightviewyoubulk);


//
       ///////////////////API CALL

        if (MainActivity.count == 1) {
            String steps = RedirectActivity.number_steps;
            TextView steps1 = this.findViewById(R.id.fit2);
            steps1.setText(steps);

            String hearts = RedirectActivity.ActiveCal;
            TextView heart1 = this.findViewById(R.id.calView);
            heart1.setText(hearts);

            String mins = RedirectActivity.ActiveMins;
            TextView mins1 = this.findViewById(R.id.minsview);
            mins1.setText(mins);

        }
        else {

        }
    }


        public void buttonSave (View view){

            String name = etnamebulk.getText().toString();
            int ages = Integer.parseInt(etagebulk.getText().toString());
            int heights = Integer.parseInt(etheightbulk.getText().toString());

            int weights = Integer.parseInt(etweightbulk.getText().toString());


            boolean status = dbHandler.updateData(name, ages, heights, weights);


            if (status)
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "failed, update all values", Toast.LENGTH_SHORT).show();

            Intent refresh = new Intent(this, Dashboardbulk.class);
            startActivity(refresh);
            this.finish();


        }


        public void buttonsatDashyoubulk (View view){
//
//        //        button with switch for views!
//         use count to

            switch (view.getId()) {

                case R.id.youButtonyoubulk:
                    //code
                    Intent youpage = new Intent(this, YouBulk.class);
                    startActivity(youpage);
                    //count = 1;

                    break;

                case R.id.dashButtonyoubulk:

                    Intent dashpage = new Intent(this, Dashboardbulk.class);
                    startActivity(dashpage);
                    //count = 2;
                    break;

                case R.id.planButtonyoubulk:
                    Intent planpage = new Intent(this, PlanShred.class);
                    startActivity(planpage);
                    break;

                case R.id.setButtonyoubulk:
                    Intent setttingspage = new Intent(this, SettingsShred.class);
                    startActivity(setttingspage);
                    break;


            }

        }
    }
